﻿namespace NiceHashBotLib.Enum
{
    public enum AlgorithmType : int
    {
        Scrypt = 0,
        Sha256 = 1,
        X11 = 3,
        X13 = 4,
        NeoScrypt = 8,
        Qubit = 11,
        Quark = 12,
        Lyra2REv2 = 14,
        CryptoNight = 22,
        X16R = 33,
    }
}
