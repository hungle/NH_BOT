﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NiceHashBotLib.Enum;
using NLog;

namespace NiceHashBotLib
{
    public class OrderInstance
    {
        #region PRIVATE_PROPERTIES
        public string GuidId { get; set; }
        public string CoinLabel { get; }
        public int ServiceLocation { get; }
        public int Algorithm { get;}
        public int OrderId { get; private set; }
        public double MaxPrice { get; private set; }
        public double StartLimit { get; private set; }
        public Pool PoolData { get;}
        public bool CanRun { get; private set; }
        private Thread _orderThread;
        public Order LastOrderStats { get; private set; }
        public double StartingPrice { get; private set; }
        public double CurrentPrice { get; private set; }
        public double StartingAmount { get; private set; }
        public bool IsRefill { get; set; }
        public bool IsFixedPrice { get; set; }
        public DateTime DecreaseTime { get; private set; }

        #endregion

        #region PUBLIC_METHODS
        private static readonly Logger LoggerInfo = LogManager.GetLogger("info");

        /// <summary>
        /// Create new order instance. This instance actively monitors order and adjusts price on-fly to keep order competitive at all times.
        /// </summary>
        /// <param name="sl">Service location; 0 for NiceHash, 1 for WestHash.</param>
        /// <param name="algo">Algorithm number.</param>
        /// <param name="maximalPrice">Maximal allowed order price.</param>
        /// <param name="limit">Order limit in GH/s (TH/s for Algorithm 1 - SHA256). 0 for unlimited.</param>
        /// <param name="poolInfo">Pool information.</param>
        /// <param name="id">Optional - If monitoring existing order, set this to order ID.</param>i
        /// <param name="price">Optional starting price.</param>
        /// <param name="StartingAmount">Optional starting amount in BTC.</param>
        public OrderInstance(string coinLabel, int sl, double maximalPrice, double limit, Pool poolInfo, double price = 0.001, double amount = 0.01, bool refill = false, bool isFixedPrice = false)
        {
            CanRun = true;
            ServiceLocation = sl;
            Algorithm = poolInfo.Algorithm;
            MaxPrice = maximalPrice;
            StartLimit = limit;
            PoolData = poolInfo;
            StartingPrice = price;
            CurrentPrice = price;
            StartingAmount = amount;
            IsRefill = refill;
            DecreaseTime = DateTime.Now - APIWrapper.PRICE_DECREASE_INTERVAL;
            IsFixedPrice = isFixedPrice;
            CoinLabel = coinLabel;
            OrderId = 0; // neworder
        }

        public void StartOrder()
        {
            _orderThread = new Thread(ThreadRun);
            _orderThread.Start();
        }



        /// <summary>
        /// Set new maximal price for this order instance.
        /// </summary>
        /// <param name="newMaxPrice">Maximal allowed price.</param>
        public void SetMaximalPrice(double newMaxPrice)
        {
            lock (this)
            {
                MaxPrice = newMaxPrice;
            }
        }

        
        /// <summary>
        /// Set new current price for this order instance.
        /// </summary>
        /// <param name="newMaxPrice">Maximal allowed price.</param>
        public void SetCurrentPrice(double newCurrentPrice)
        {
            lock (this)
            {
                CurrentPrice = newCurrentPrice;
            }
        }

        /// <summary>
        /// Set new limit for this order instance.
        /// </summary>
        /// <param name="limit">New order limit in GH/s (TH/s for Algorithm 1 - SHA256). 0 for unlimited.</param>
        public void SetLimit(double limit)
        {
            lock (this)
            {
                StartLimit = limit;
            }
        }

        /// <summary>
        /// Stop this order instance monitoring.
        /// </summary>
        public bool Stop()
        {
            var result = false;
            lock (this)
            {
                var cn = 1;
                while (!result && cn < 5)
                {
                    if (OrderId != 0)
                        result = APIWrapper.OrderRemove(ServiceLocation, Algorithm, OrderId);
                    else
                    {
                        result = true;
                        break;
                    }
                    cn++;
                    if (result)
                    {
                        break;
                    }
                    Thread.Sleep(2000);
                }

                CanRun = false;
            }

            _orderThread.Join();
            return result;
        }
        
        #endregion

        #region PRIVATE_METHODS

        private void ThreadRun()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            while (CanRun)
            {
                if (!APIWrapper.ValidAuthorization)
                {
                    System.Threading.Thread.Sleep(500);
                    continue;
                }

                lock (this)
                {

                    // Verify, if we have order.
                    if (OrderId == 0)
                    {
                        // Need to create order.
                        OrderId = APIWrapper.OrderCreate(ServiceLocation, StartingAmount, StartingPrice, StartLimit, PoolData);
                        if (OrderId > 0)
                        {
                            LibConsole.WriteLine(LibConsole.TEXT_TYPE.INFO, "Created new order #" + OrderId);
                            LoggerInfo.Info($"--- Create Order {CoinLabel} - {((ServerType)ServiceLocation).ToString()} {((AlgorithmType)Algorithm).ToString()} - #{OrderId} - Limit: {StartLimit} - MaxPrice: {MaxPrice} " +
                                        $"- IsFixedPrice: {IsFixedPrice} - IsRefill: {IsRefill} ");
                        }
                    }

                    if (OrderId > 0)
                    {
                        // Get all orders.
                        List<Order> allOrders = APIWrapper.GetAllOrders(ServiceLocation, Algorithm, true);

                        if (allOrders != null)
                        {
                            // Find our order.
                            Order myOrder = allOrders.SingleOrDefault(a => a.ID == OrderId);
                            // Get total hashing speed
                            double ts = APIWrapper.GetTotalSpeed(ServiceLocation, Algorithm);

                            if (myOrder != null)
                            {
                                ProcessMyOrder(myOrder, allOrders.ToArray(), ts, IsRefill);
                                LastOrderStats = new Order(myOrder);
                            }
                        }
                    }
                }

                // Wait 10 seconds.
                for (int i = 0; i < 10; i++)
                {
                    if (!CanRun) break;
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        private void ProcessMyOrder(Order myOrder, Order[] allOrders, double totalSpeed, bool isRefill = false)
        {
            // Change limit if requested by user.
            if (Math.Abs(myOrder.SpeedLimit - StartLimit) >= 0.01)
            {
                LibConsole.WriteLine(LibConsole.TEXT_TYPE.INFO, "Changing limit order #" + myOrder.ID + " to " + StartLimit.ToString("F2"));
                LoggerInfo.Info($"Changing limit order {CoinLabel} #{myOrder.ID} to " + StartLimit.ToString("F2"));
                myOrder.SetLimit(StartLimit);
            }

            // Check if refill is needed.
            if (myOrder.BTCAvailable <= 0.002 && isRefill)
            {
                LibConsole.WriteLine(LibConsole.TEXT_TYPE.INFO, "Refilling order #" + myOrder.ID);
                LoggerInfo.Info($"Refilling order {CoinLabel} #{myOrder.ID} ");
                if (myOrder.Refill(0.005))
                    myOrder.BTCAvailable += 0.005;
            }

            // Do not adjust price, if order is dead.
            if (!myOrder.Alive) return;

            // Adjust price.
            // Only not a static order
            if (!IsFixedPrice)
            {
                double minimalPrice = GetMinimalNeededPrice(allOrders, totalSpeed);
                if (!IncreasePrice(myOrder, allOrders, minimalPrice))
                    DecreasePrice(myOrder, allOrders, minimalPrice);
            }
        }


        private double GetMinimalNeededPrice(Order[] allOrders, double totalSpeed)
        {
            double totalWantedSpeed = 0;
            int i;
            //double Multi = 1;
            //if (Algorithm == 1) Multi = 1000;
            for (i = 0; i < allOrders.Length; i++)
            {
                if (allOrders[i].SpeedLimit == 0)
                    totalWantedSpeed += 1000000000;
                else
                    totalWantedSpeed += allOrders[i].SpeedLimit / APIWrapper.ALGORITHM_MULTIPLIER[Algorithm];

                if (totalWantedSpeed > totalSpeed) break;
            }

            if (i == allOrders.Length)
                i = allOrders.Length - 1;

            return (allOrders[i].Price + 0.0001);
        }


        private bool IncreasePrice(Order myOrder, Order[] allOrders, double minimalPrice)
        {
            // Do not make price higher if we are already on top of the list (first alive).
            // Consider fixed orders.
            foreach (Order o in allOrders)
            {
                if (!o.Alive) continue;
                if (o.OrderType == 1) continue;
                if (o == myOrder) return false;
                else break;
            }

            if (CurrentPrice > myOrder.Price)
            {
                double newP = myOrder.SetPrice(CurrentPrice);
                if (newP > 0)
                {
                    myOrder.Price = newP;
                    CurrentPrice = newP;
                }
            }

            // Do not increase price, if we already have price higher or equal compared to minimal price.
            if (myOrder.Price >= (minimalPrice - 0.0001)) return false;


            if (MaxPrice >= minimalPrice)
            {
                // We can set higher price.
                LibConsole.WriteLine(LibConsole.TEXT_TYPE.INFO, "Setting price order #" + myOrder.ID + " to " + minimalPrice.ToString("F4"));
                LoggerInfo.Info($"Setting price order {CoinLabel} #{myOrder.ID} to {minimalPrice.ToString("F4")}. Current Speed: {(LastOrderStats?.Speed ?? -1 * APIWrapper.ALGORITHM_MULTIPLIER[Algorithm]).ToString("F4")}");
                double newP = myOrder.SetPrice(minimalPrice);
                if (newP > 0)
                {
                    myOrder.Price = newP;
                    CurrentPrice = newP;
                }

                return true;
            }
            //else if (myOrder.Price < MaxPrice)
            //{
            //    // We can at least set price to be MaxPrice
            //    LibConsole.WriteLine(LibConsole.TEXT_TYPE.INFO, "Setting price order #" + myOrder.ID + " to " + MaxPrice.ToString("F4"));
            //    double newP = myOrder.SetPrice(MaxPrice);
            //    if (newP > 0)
            //    {
            //        myOrder.Price = newP;
            //        CurrentPrice = newP;
            //    }

            //    return true;
            //}

            return false;
        }


        private void DecreasePrice(Order myOrder, Order[] allOrders, double minimalPrice)
        {
            // Check time if decrase is possible.
            if (DecreaseTime + APIWrapper.PRICE_DECREASE_INTERVAL > DateTime.Now) return;

            // Decrease only in case if we are still above or equal to minimal price. Or if we are above maximal price.
            if (myOrder.Price + APIWrapper.PRICE_DECREASE_STEP[myOrder.Algorithm] >= minimalPrice ||
                myOrder.Price > MaxPrice)
            {
                double newP = myOrder.SetPriceDecrease();
                if (newP > 0)
                {
                    myOrder.Price = newP;
                    CurrentPrice = newP;
                    LibConsole.WriteLine(LibConsole.TEXT_TYPE.INFO, "Decreasing price order #" + myOrder.ID + " to " + newP.ToString("F4"));
                    LoggerInfo.Info($"Decreasing price order {CoinLabel} #{myOrder.ID} to " + newP.ToString("F4"));
                    DecreaseTime = DateTime.Now;
                }
            }
        }

        #endregion
    }
}
