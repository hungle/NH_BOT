﻿using BitcoinWrapper.Wrapper;
using BitcoinWrapper.Wrapper.Interfaces;
using NiceHashBot.Model;
using NiceHashBotLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;
using NiceHashBot.Logging;
using NLog.Config;
using NLog.Targets;
using NLog;

namespace NiceHashBot
{
    static class Program
    {
        public static LoggingConfiguration NlogConfig = new LoggingConfiguration();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            NlogConfig.LoggingRules.Clear();
            NlogConfig.SetInfoConfig();
            NlogConfig.SetErrorConfig();
            PoolContainer.Initialize();
            SettingsContainer.Initialize();
            OrderContainer.Initialize();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
            //BotRun();
            //Console.ReadLine();
            OrderContainer.Deinitialize();
        }

        
    }
}
