﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BitcoinWrapper.Wrapper.Interfaces;
using NiceHashBot.Model;
using NiceHashBotLib;

namespace NiceHashBot.Process
{

    public static class CoinHelper
    {
        private static readonly string _maxDiffAccess;
        private static IBaseBtcConnector _baseConnector;
        private static int _numList = 25;
        private static int _currentCount;
        private static int _prevCount;
        private static List<BlockInfo> _list1;



        public static void RemoveAllOrderAgain(string poolLabel)
        {
            try
            {
                var pools = PoolContainer.GetAll();
                var pool = pools.SingleOrDefault(p => p.Label == poolLabel);
                List<Order> allEuOrders = APIWrapper.GetMyOrders(0, 0).Where(o => pool != null && (o.PoolHost == pool.Host && o.PoolPort == pool.Port)).ToList();
                List<Order> allUsOrders = APIWrapper.GetMyOrders(1, 0).Where(o => pool != null && (o.PoolHost == pool.Host && o.PoolPort == pool.Port)).ToList();

                foreach (var order in allEuOrders)
                {
                    APIWrapper.OrderRemove(order.ServiceLocation, order.Algorithm, order.ID);
                }
                foreach (var order in allUsOrders)
                {
                    APIWrapper.OrderRemove(order.ServiceLocation, order.Algorithm, order.ID);
                }
            }
            catch (Exception ex)
            {

            }
        }


        public static List<BlockInfo> GetListBlock(int crrCount, int numList)
        {
            var listBlock = new List<BlockInfo>();
            int count = 0;
            int i = 0;
            //for (int i = numList - 1; i >= 0; i--)
            while (count < numList)
            {
                var hash = _baseConnector.GetBlockhash(crrCount - i);
                var block = _baseConnector.GetBlock(hash);
                //pow of stack ---> continue
                if (block.Difficulty < 1)
                {
                    i++;
                    continue;
                }
                var blockInfo = new BlockInfo()
                {
                    BlockNumber = crrCount - i,
                    Hash = hash,
                    ServerTimeStamp = block.Time,
                    Diff = block.Difficulty
                };
                listBlock.Add(blockInfo);
                i++;
                count++;
            }
            listBlock.Reverse();
            return listBlock;
        }
        public static bool CheckDGWAfterNBlock(List<BlockInfo> blocks, int n, double maxAcceptDiff)
        {
            var timenow = DateTime.Now;
            var timeStampTimeNowSecond = (int)(timenow.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
            for (int i = 0; i < n; i++)
            {
                timeStampTimeNowSecond += i * 30;
                var nextDiff = DGW(blocks);
                if (nextDiff > maxAcceptDiff)
                {
                    return false;
                }
                blocks = blocks.Skip(1).ToList();
                blocks.Add(new BlockInfo()
                {
                    Diff = nextDiff,
                    ServerTimeStamp = timeStampTimeNowSecond
                });
            }
            return true;
        }

        public static double DGW(List<BlockInfo> blocks)
        {

            double nActualTimespan = 0;
            //nActualTimespan = blocks[numList - 1].ServerTimeStamp - blocks[0].ServerTimeStamp;
            double sumTargets = 0.0;
            for (int i = 0; i < 24; i++)
            {
                if (i > 0)
                {
                    nActualTimespan += blocks[i].ServerTimeStamp - blocks[i - 1].ServerTimeStamp;
                }
                sumTargets += blocks[i].Diff;
                if (i == blocks.Count - 1)
                {
                    sumTargets += blocks[i].Diff;
                }
            }
            double nTargetTimespan = 24 * 60;
            if (nActualTimespan < nTargetTimespan / 3.0)
                nActualTimespan = nTargetTimespan / 3.0;
            if (nActualTimespan > nTargetTimespan * 3.0)
                nActualTimespan = nTargetTimespan * 3.0;

            double compact = (sumTargets / (24 + 1)) * (nTargetTimespan / nActualTimespan);
            //double compact = (sumTargets / numList) * (nTargetTimespan / nActualTimespan);

            return compact;
        }

    }
}
