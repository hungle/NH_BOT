﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace NiceHashBot.Logging
{
    public static class LoggingExtension
    {
        public static void SetInfoConfig(this LoggingConfiguration config)
        {
            var NLogFolder = ConfigurationManager.AppSettings.Get("NLogFolder");

            var target =
                new FileTarget
                {
                    FileName = NLogFolder + "${shortdate}-Info.log",
                    Layout = "${longdate} ${uppercase:${level}} ${message} ${exception:format=tostring}",
                    Name = "info"
                };

            config.AddTarget("info", target);

            var rule = new LoggingRule("info", LogLevel.Info, target);

            config.LoggingRules.Add(rule);
            LogManager.Configuration = config;
            LogManager.Configuration.Reload();
        }
        public static void SetErrorConfig(this LoggingConfiguration config)
        {
            var NLogFolder = ConfigurationManager.AppSettings.Get("NLogFolder");

            var target =
                new FileTarget
                {
                    FileName = NLogFolder + "${shortdate}-Error.log",
                    Layout = "${longdate} ${uppercase:${level}} ${message} ${exception:format=tostring}",
                    Name = "error"
                };

            config.AddTarget("error", target);

            var rule = new LoggingRule("error", LogLevel.Error, target);

            config.LoggingRules.Add(rule);
            LogManager.Configuration = config;
            LogManager.Configuration.Reload();
        }
        public static void SetUpNLogTrace(this LoggingConfiguration config,string coinLabel)
        {
            var NLogFolder = ConfigurationManager.AppSettings.Get("NLogFolder");

            var target =
                new FileTarget
                {
                    FileName = NLogFolder + "${shortdate}-Trace-" + coinLabel + ".log",
                    Layout = "${longdate} ${uppercase:${level}} - " + coinLabel + " - ${message} ${exception:format=tostring}",
                    Name = coinLabel
                };

            config.AddTarget(coinLabel, target);

            var rule = new LoggingRule(coinLabel, LogLevel.Trace, target);

            config.LoggingRules.Add(rule);
            LogManager.Configuration = config;
            LogManager.Configuration.Reload();
        }
    }
}
