﻿using BitcoinWrapper.Data;
using BitcoinWrapper.Wrapper;
using BitcoinWrapper.Wrapper.Interfaces;
using BitSharp.Common;
using NiceHashBot.Helper;
using NiceHashBot.Model;
using NiceHashBotLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NiceHashBot.Logging;
using NiceHashBotLib.Enum;
using NLog;
using NLog.Config;
using NLog.Targets;
using Timer = System.Windows.Forms.Timer;

namespace NiceHashBot
{
    public partial class FormMain : Form
    {

        private Timer TimerRefresh;
        private Timer BalanceRefresh;
        private Timer MainBotRefresh;

        private List<CurrentBlock> _currentBlockList = new List<CurrentBlock>();
        private List<BlocksCached> _blocksCached = new List<BlocksCached>();

        private Dictionary<string, Thread> botTimerList = new Dictionary<string, Thread>();


        private string[] ListDGWCoins { get; set; }
        private string[] ListForkCoins { get; set; }
        private string[] ListEsrcCoins { get; set; }
        private string[] ListDynamicCoins { get; set; }
        private string[] ListReviewCoins { get; set; }
        private string[] ListOrderCoins { get; set; }

        private double _balance;

        private static readonly Logger LoggerInfo = LogManager.GetLogger("info");
        private static readonly Logger LoggerError = LogManager.GetLogger("error");

        public FormMain()
        {

            InitializeComponent();
            this.Text = Application.ProductName + " [v" + Application.ProductVersion + "]";

            Thread threadTimerRefresh = new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        Thread.Sleep(1000);
                        TimerRefresh_Tick();
                    }
                    catch (Exception e)
                    {
                        //Logger.Error(e, "threadTimerRefresh");
                        //LibConsole.WriteLine(LibConsole.TEXT_TYPE.ERROR, e.Message);
                    }
                }
            });

            Thread threadBalance = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(10000);
                    BalanceRefresh_Tick();
                }
            });

            Thread threadMainbot = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(5000);
                    try
                    {
                        MainBot_Tick();
                    }
                    catch (Exception e)
                    {
                        LibConsole.WriteLine(LibConsole.TEXT_TYPE.ERROR, e.Message);
                        LoggerError.Error(e, "threadMainbot");
                    }
                }
            });

            threadTimerRefresh.Start();
            threadBalance.Start();
            threadMainbot.Start();


        }

        private void MainBot_Tick()
        {
            ConfigurationManager.RefreshSection("appSettings");
            PoolContainer.Initialize();

            ListDGWCoins = ConfigurationManager.AppSettings.Get("DGWList").Split(';');
            ListForkCoins = ConfigurationManager.AppSettings.Get("ForkList").Split(';');
            ListEsrcCoins = ConfigurationManager.AppSettings.Get("EsrcList").Split(';');
            ListDynamicCoins = ConfigurationManager.AppSettings.Get("DynamicList").Split(';');
            ListReviewCoins = ConfigurationManager.AppSettings.Get("DGWReview").Split(';');
            ListOrderCoins = ConfigurationManager.AppSettings.Get("OrderList").Split(';');

            var coinsMonitor = ConfigurationManager.AppSettings.Get("CoinsMonitor").Split(';');
            var coinRemoves = botTimerList.Where(b => !coinsMonitor.Contains(b.Key)).ToList();
            var coinAdds = coinsMonitor.Where(c => botTimerList.All(b => b.Key != c)).ToList();
            foreach (var coinRemove in coinRemoves)
            {
                StopTimer(coinRemove.Key);
                var coinRule = LogManager.Configuration.LoggingRules.FirstOrDefault(r => r.NameMatches(coinRemove.Key));
                LogManager.Configuration.LoggingRules.Remove(coinRule);
                LogManager.Configuration.Reload();
            }
            foreach (var coinAdd in coinAdds)
            {
                LogManager.Configuration.SetUpNLogTrace(coinAdd);
                StartTimer(coinAdd);
            }



            var listTxtMonitor = this.panel2.Controls.OfType<RichTextBox>().Where(n => n.Name.Contains("txtMonitor")).ToList();
            foreach (var txtMonitor in listTxtMonitor)
            {
                txtMonitor.Height = this.panel2.Height / (coinsMonitor.Length);
            }



            var listTxtReview = this.panel1.Controls.OfType<RichTextBox>().Where(n => n.Name.Contains("txt")).ToList();
            foreach (var txtReview in listTxtReview)
            {
                txtReview.Height = this.panel1.Height / (ListReviewCoins.Length);
            }

            var txtBoxRemoveds = this.panel1.Controls.OfType<RichTextBox>().Where(t => !ListReviewCoins.Contains(t.Name.Replace("txt", ""))).ToList();
            foreach (var txtBoxRemoved in txtBoxRemoveds)
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke((MethodInvoker)delegate ()
                    {
                        this.panel1.Controls.Remove(txtBoxRemoved);
                        txtBoxRemoved.Dispose();
                    });
                }
                else
                {
                    this.panel1.Controls.Remove(txtBoxRemoved);
                    txtBoxRemoved.Dispose();
                }

            }


        }

        private void StartTimer(string coinLabel)
        {
            try
            {
                Thread threadHere = new Thread(() =>
                {
                    while (true)
                    {
                        try
                        {
                            Thread.Sleep(5000);
                            BotRefreshTick(coinLabel);
                        }
                        catch (Exception e)
                        {
                            LibConsole.WriteLine(LibConsole.TEXT_TYPE.ERROR, e.Message);
                            LoggerError.Error(e, $"StartTimer - threadHere: {coinLabel}");
                        }
                    }
                });
                threadHere.Start();
                botTimerList.Add(coinLabel, threadHere);
            }
            catch (Exception ex)
            {
                LibConsole.WriteLine(LibConsole.TEXT_TYPE.ERROR, ex.Message);
                LoggerError.Error(ex, $"StartTimer: {coinLabel}");
            }
        }

        private void StopTimer(string coinLabel)
        {
            try
            {
                var txtBoxRemoved = this.panel2.Controls.OfType<RichTextBox>().FirstOrDefault(t => t.Name == "txtMonitor" + coinLabel);

                if (txtBoxRemoved != null)
                {
                    if (this.InvokeRequired)
                    {
                        this.BeginInvoke((MethodInvoker)delegate ()
                        {
                            this.panel2.Controls.Remove(txtBoxRemoved);
                            txtBoxRemoved.Dispose();
                        });
                    }
                    else
                    {
                        this.panel2.Controls.Remove(txtBoxRemoved);
                        txtBoxRemoved.Dispose();
                    }
                }

                botTimerList[coinLabel].Abort();
                botTimerList.Remove(coinLabel);
            }
            catch (Exception e)
            {
                LibConsole.WriteLine(LibConsole.TEXT_TYPE.ERROR, e.Message);
                LoggerError.Error(e, $"StopTimer: {coinLabel}");
            }
        }

        private void BotRefreshTick(string coinLabel)
        {
            var coinLogger = NLog.LogManager.GetLogger(coinLabel);

            ConfigurationManager.RefreshSection("appSettings");
            var config = ConfigurationManager.AppSettings.Get(coinLabel + "_Config");
            var connector = new BaseBtcConnector(config.Split(';')[0], config.Split(';')[1], config.Split(';')[2]);
            string s = "";
            var timenow = DateTime.Now;
            //var currentBlock = GetBlock(connector);
            var blocksCached = _blocksCached.SingleOrDefault(b => b.CoinLabel == coinLabel)?.ListBlocks ?? new List<BlockInfo>();
            var currentBlock = GetLastPOWBlock(connector); // blocks.Last();

            var preBlock = _currentBlockList.FirstOrDefault(c => c.CoinLabel == coinLabel)?.BlockNumber ?? 0;
            if (preBlock != currentBlock.BlockNumber)
            {
                if (preBlock != 0)
                {
                    _currentBlockList.FirstOrDefault(c => c.CoinLabel == coinLabel).BlockNumber = currentBlock.BlockNumber;
                }
                else
                {
                    _currentBlockList.Add(new CurrentBlock
                    {
                        CoinLabel = coinLabel,
                        BlockNumber = currentBlock.BlockNumber
                    });
                }
                coinLogger.Trace("New block : " + currentBlock.BlockNumber + " - Diff: " + currentBlock.Diff);
                s += timenow.ToString("HH:mm:ss") + " - " + coinLabel + " - New block : " + currentBlock.BlockNumber + " - Diff: " +
                     currentBlock.Diff + Environment.NewLine;
            }


            double minDiff = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MinDiff"));
            double maxDiff = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxDiff"));

            if (minDiff > maxDiff)
            {
                LibConsole.WriteLine(LibConsole.TEXT_TYPE.ERROR, coinLabel + "mindiff > maxdiff ----> STOPPPPPPPPPPPPPPPP!");
                LoggerError.Error($"{coinLabel} mindiff({minDiff})  > maxdiff({maxDiff}) ----> STOPPPPPPPPPPPPPPPP!");
                return;
            }


            // If static order and didn't created yet, then create it 
            if (ListOrderCoins.Contains(coinLabel) && ListDynamicCoins.Contains(coinLabel) && !OrderContainer.IsExist(coinLabel))
            {
                CreateOrder(coinLabel);
            }

            if (ListDGWCoins.Contains(coinLabel) || ListForkCoins.Contains(coinLabel) || ListEsrcCoins.Contains(coinLabel)) // DGW
            {
                double nextDiff = 0.0;
                double diffExpected = 0.0;
                var timePerBlockSecond = 1 * 60;
                double nTargetTimespan = 0.0;
                double nTargetSpacing = 0.0;
                if (ListForkCoins.Contains(coinLabel) || ListEsrcCoins.Contains(coinLabel))
                {
                    nTargetTimespan = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_Timespan"));
                    nTargetSpacing = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_Spacing"));
                }
                else
                {
                    try
                    {
                        timePerBlockSecond = Int16.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_DGWTime"));
                    }
                    catch (Exception ex)
                    {
                    }
                }
                // Incase Fork we only need 2 last block to calculate
                var listNum = ListDGWCoins.Contains(coinLabel) ? 24 : 2;
                if (ListEsrcCoins.Contains(coinLabel))
                {
                    listNum = (int)Math.Round(nTargetTimespan / nTargetSpacing);
                }
                var blocks = FillList(connector, connector.GetBlockCount(), listNum, blocksCached);
                if (blocksCached.Count == 0)
                {
                    _blocksCached.Add(new BlocksCached
                    {
                        CoinLabel = coinLabel,
                        ListBlocks = blocks
                    });
                }
                else
                {
                    _blocksCached.SingleOrDefault(b => b.CoinLabel == coinLabel).ListBlocks = blocks;
                }


                if (ListDGWCoins.Contains(coinLabel))
                {

                    nextDiff = DGW(blocks, timePerBlockSecond);

                }
                else if (ListForkCoins.Contains(coinLabel))
                {

                    // Fork Info
                    //nTargetTimespan = 3 * 15 * 60-- > 45 minutes
                    //nTargetSpacing = 5 * 60-- > 5 minutes
                    //nInterval = nTargetTimespan / nTargetSpacing = 45 / 5 = 9
                    nextDiff = ForkTarget(blocks, nTargetTimespan, nTargetSpacing, coinLabel == "Wthc");
                }
                else if (ListEsrcCoins.Contains(coinLabel))
                {

                    // Fork Info
                    //nTargetTimespan = 3 * 15 * 60-- > 45 minutes
                    //nTargetSpacing = 5 * 60-- > 5 minutes
                    //nInterval = nTargetTimespan / nTargetSpacing = 45 / 5 = 9
                    nextDiff = EsrcTarget(blocks, nTargetTimespan, nTargetSpacing);
                }

                var timeStampTimeNowSecond = (int)(timenow.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;

                var lastImagineBlock = new BlockInfo
                {
                    Diff = nextDiff,
                    ServerTimeStamp = timeStampTimeNowSecond
                };

                if (coinLabel.ToLower() == "wthc")
                {
                    lastImagineBlock.ServerTimeStamp = timeStampTimeNowSecond + 600; // 10 minute
                }

                var list2 = blocks.Skip(1).ToList();
                list2.Add(lastImagineBlock);


                if (ListDGWCoins.Contains(coinLabel))
                {
                    diffExpected = DGW(list2, timePerBlockSecond);
                }
                else if (ListForkCoins.Contains(coinLabel))
                {
                    diffExpected = ForkTarget(list2, nTargetTimespan, nTargetSpacing, coinLabel == "Wthc");
                }
                else if (ListEsrcCoins.Contains(coinLabel))
                {
                    diffExpected = EsrcTarget(list2, nTargetTimespan, nTargetSpacing);
                }

                if (ListReviewCoins.Contains(coinLabel))
                {
                    var s2 = timenow.ToString("HH:mm:ss") + " - " + coinLabel + " - Block : " +
                             currentBlock.BlockNumber + " - Diff: " +
                             currentBlock.Diff + " -- NextDiff: " + nextDiff + " -- Next2Diff: " + diffExpected +
                             Environment.NewLine;
                    var thisTxtBox = this.panel1.Controls.OfType<RichTextBox>().FirstOrDefault(t => t.Name == "txt" + coinLabel);
                    if (thisTxtBox == null)
                    {
                        if (this.InvokeRequired)
                        {
                            this.BeginInvoke((MethodInvoker)delegate ()
                            {
                                RichTextBox txtBox = new RichTextBox();
                                txtBox.Name = "txt" + coinLabel;
                                txtBox.Multiline = true;
                                txtBox.Visible = true;
                                txtBox.Dock = DockStyle.Top;
                                txtBox.Margin = new Padding(10);
                                txtBox.HideSelection = false;
                                this.panel1.Controls.Add(txtBox);
                            });
                        }
                        else
                        {
                            RichTextBox txtBox = new RichTextBox();
                            txtBox.Name = "txt" + coinLabel;
                            txtBox.Multiline = true;
                            txtBox.Visible = true;
                            txtBox.Dock = DockStyle.Top;
                            txtBox.Margin = new Padding(10);
                            txtBox.HideSelection = false;
                            this.panel1.Controls.Add(txtBox);
                        }

                    }
                    else
                    {
                        thisTxtBox.Invoke((Action)delegate
                        {
                            thisTxtBox.AppendText(s2);
                        });
                    }
                }

                if ((ListForkCoins.Contains(coinLabel) || ListEsrcCoins.Contains(coinLabel)) && diffExpected < minDiff && (!OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel)))
                {
                    if (ListOrderCoins.Contains(coinLabel))
                    {
                        if (ListDynamicCoins.Contains(coinLabel))
                        {
                            ChangeOrderToMaxHash(coinLabel, out var firstChange);
                            s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to Maxhash " + Environment.NewLine;
                            if (firstChange)
                            {
                                coinLogger.Trace($" Change to Maxhash");
                            }
                        }
                        else if (!OrderContainer.IsExist(coinLabel) && _balance >= 0.005)
                        {
                            CreateOrder(coinLabel);
                            s += timenow.ToString("HH:mm:ss") + "  Start " + coinLabel + " Order" + Environment.NewLine;
                            coinLogger.Trace($" Start Order");
                        }
                    }
                }
                else if (ListDGWCoins.Contains(coinLabel) && CheckDGWAfterNBlock(list2, 5, minDiff, timePerBlockSecond) && (!OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel))
                         )
                {
                    #region DGW Order
                    if (ListOrderCoins.Contains(coinLabel))
                    {
                        if (ListDynamicCoins.Contains(coinLabel))
                        {
                            ChangeOrderToMaxHash(coinLabel, out var firstChange);
                            s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to Maxhash " + Environment.NewLine;
                            if (firstChange)
                            {
                                coinLogger.Trace($" Change to Maxhash");
                            }
                        }
                        else if (!OrderContainer.IsExist(coinLabel) && _balance >= 0.005)
                        {
                            CreateOrder(coinLabel);
                            s += timenow.ToString("HH:mm:ss") + "  Start " + coinLabel + " Order" + Environment.NewLine;
                            coinLogger.Trace($" Start Order");
                        }
                    }
                    #endregion
                }
                else if (nextDiff > maxDiff && diffExpected > maxDiff && (OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel)))
                {
                    if (ListDynamicCoins.Contains(coinLabel))
                    {
                        ChangeOrderToMinHash(coinLabel, out var firstChange);
                        s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to MIN Hash " + Environment.NewLine;
                        if (firstChange)
                        {
                            coinLogger.Trace($" Change to Minhash");
                        }
                    }
                    else
                    {
                        OrderContainer.RemoveAll(coinLabel);
                        s += timenow.ToString("HH:mm:ss") + "  Remove " + coinLabel + " Order" + Environment.NewLine;
                        coinLogger.Trace($" Remove Order");
                        SetColorCoinlabel(coinLabel, Color.White);
                    }
                }

                if (nextDiff > maxDiff && diffExpected > maxDiff && (!OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel)))
                {
                    if (ListDynamicCoins.Contains(coinLabel))
                    {
                        ChangeOrderToMinHash(coinLabel, out var firstChange);
                        s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to MIN Hash " + Environment.NewLine;
                    }
                    else
                    {
                        OrderContainer.RemoveAll(coinLabel);
                    }
                }
            }
            else
            {
                if (currentBlock.Diff < Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MinDiff")) &&
                    (!OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel)))
                {
                    if (ListOrderCoins.Contains(coinLabel))
                    {
                        if (ListDynamicCoins.Contains(coinLabel))
                        {
                            ChangeOrderToMaxHash(coinLabel, out var firstChange);
                            s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to Maxhash " + Environment.NewLine;
                            if (firstChange)
                            {
                                coinLogger.Trace($" Change to Maxhash");
                            }
                        }
                        else if (!OrderContainer.IsExist(coinLabel) && _balance >= 0.005)
                        {
                            CreateOrder(coinLabel);
                            s += timenow.ToString("HH:mm:ss") + "  Start " + coinLabel + " Order" + Environment.NewLine;
                            coinLogger.Trace($" Start Order");
                        }
                    }
                }
                else if (currentBlock.Diff > Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxDiff")) &&
                         (OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel)))
                {
                    if (ListDynamicCoins.Contains(coinLabel))
                    {
                        ChangeOrderToMinHash(coinLabel, out var firstChange);
                        s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to MIN Hash " + Environment.NewLine;
                        if (firstChange)
                        {
                            coinLogger.Trace($" Change to Minhash");
                        }
                    }
                    else
                    {
                        OrderContainer.RemoveAll(coinLabel);
                        s += timenow.ToString("HH:mm:ss") + "  Remove " + coinLabel + " Order" + Environment.NewLine;
                        coinLogger.Trace($" Remove Order");
                        SetColorCoinlabel(coinLabel, Color.White);
                    }
                }

                if (currentBlock.Diff > Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxDiff")) &&
                    (!OrderContainer.IsExist(coinLabel) || ListDynamicCoins.Contains(coinLabel)))
                {
                    if (ListDynamicCoins.Contains(coinLabel))
                    {
                        ChangeOrderToMinHash(coinLabel, out var firstChange);
                        s += !firstChange ? "" : timenow.ToString("HH:mm:ss") + " Change to MIN Hash " + Environment.NewLine;
                    }
                    else
                    {
                        OrderContainer.RemoveAll(coinLabel);
                    }
                }
            }


            if (s != "")
            {

                var thisTxtBox = this.panel2.Controls.OfType<RichTextBox>().FirstOrDefault(t => t.Name == "txtMonitor" + coinLabel);
                if (thisTxtBox == null)
                {
                    if (this.InvokeRequired)
                    {
                        this.BeginInvoke((MethodInvoker)delegate ()
                        {
                            RichTextBox txtBox = new RichTextBox();
                            txtBox.Name = "txtMonitor" + coinLabel;
                            txtBox.Multiline = true;
                            txtBox.Visible = true;
                            txtBox.Dock = DockStyle.Top;
                            txtBox.HideSelection = false;
                            this.panel2.Controls.Add(txtBox);
                            txtBox.AppendText(s);
                        });
                    }
                    else
                    {
                        RichTextBox txtBox = new RichTextBox();
                        txtBox.Name = "txtMonitor" + coinLabel;
                        txtBox.Multiline = true;
                        txtBox.Visible = true;
                        txtBox.Dock = DockStyle.Top;
                        txtBox.HideSelection = false;
                        this.panel2.Controls.Add(txtBox);
                        txtBox.AppendText(s);
                    }
                }
                else
                {
                    thisTxtBox.Invoke((Action)delegate
                    {
                        thisTxtBox.AppendText(s);
                    });
                }
                //var filename = "orderLog" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
                //if (!File.Exists(filename))
                //{
                //    using (var stream = File.Create(filename)) { }
                //}
                //File.AppendAllText(filename, s);
            }

        }

        private bool SetColorCoinlabel(string coinLabel, Color color)
        {
            var monitorTxtBox = this.panel2.Controls.OfType<RichTextBox>()
                .FirstOrDefault(t => t.Name == "txtMonitor" + coinLabel);
            var reviewTxtBox = this.panel1.Controls.OfType<RichTextBox>().FirstOrDefault(t => t.Name == "txt" + coinLabel);
            var result = false;
            if (monitorTxtBox != null)
            {
                monitorTxtBox.Invoke((Action)delegate
                {
                    if (monitorTxtBox.BackColor != color)
                    {
                        monitorTxtBox.BackColor = color;
                        result = true;
                    }
                });
            }
            if (reviewTxtBox != null)
            {
                reviewTxtBox.Invoke((Action)delegate
                {
                    if (reviewTxtBox.BackColor != color)
                    {
                        reviewTxtBox.BackColor = color;
                        result = true;
                    }
                });
            }

            return false;
        }


        private void ChangeOrderToMaxHash(string coinLabel, out bool firstChange)
        {
            var maxHash = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxHash"));
            var numberOfOrder = OrderContainer.GetAll(coinLabel).Count;
            maxHash /= numberOfOrder;
            maxHash = Math.Round(maxHash, 2);
            OrderContainer.SetLimit(coinLabel, maxHash);
            firstChange = SetColorCoinlabel(coinLabel, Color.Coral);
        }
        private void ChangeOrderToMinHash(string coinLabel, out bool firstChange)
        {
            var minHash = !string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get(coinLabel + "_IdleHash")) ? Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_IdleHash")) : 0.01;
            OrderContainer.SetLimit(coinLabel, minHash);
            firstChange = SetColorCoinlabel(coinLabel, Color.BurlyWood);
        }


        private void CreateOrder(string coinLabel)
        {
            ConfigurationManager.RefreshSection("appSettings");
            string _poolLabel = ConfigurationManager.AppSettings.Get(coinLabel + "_PoolChoose");
            var pool = PoolContainer.FindPool(_poolLabel);
            var maxHash = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxHash"));

            var startAmount = 0.005;
            try
            {
                startAmount = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_StartAmount"));
            }
            catch (Exception ex)
            {
            }

            var refill = false;
            try
            {
                refill = Boolean.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_Refill"));
            }
            catch (Exception ex)
            {
            }
            if (_balance < 0.005)
            {
                return;
            }
            var minMax = ConfigurationManager.AppSettings.Get(((AlgorithmType)pool.Algorithm).ToString());
            var startPrice = double.Parse(minMax.Split(';')[0]);
            var price = double.Parse(minMax.Split(';')[1]);
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get(coinLabel + "_StartPrice")))
            {
                startPrice = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_StartPrice"));
            }
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxPrice")))
            {
                price = Double.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_MaxPrice"));
            }
            if (price < startPrice)
            {
                price = startPrice + 1;
            }


            int numberOrder = 1;
            try
            {
                numberOrder = int.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_NumberOrder"));
            }
            catch (Exception e)
            {
            }

            if (numberOrder > (int)(_balance / 0.005))
            {
                numberOrder = (int)(_balance / 0.005);
            }




            var isFixedPrice = false;
            try
            {
                isFixedPrice = bool.Parse(ConfigurationManager.AppSettings.Get(coinLabel + "_IsFixedPrice"));
            }
            catch (Exception e)
            {
            }


            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get(coinLabel + "_ListOrder")))
            {
                //CoinLabel_ListOrder: 0_Price;0_Price;0_Price;1_Price   : 0 = EU , 1 = US
                var listStaticOrder = ConfigurationManager.AppSettings.Get(coinLabel + "_ListOrder").Split(';');
                maxHash /= listStaticOrder.Length;
                maxHash = Math.Round(maxHash, 2);
                foreach (var order in listStaticOrder)
                {
                    var orderInstance = new OrderInstance(coinLabel, int.Parse(order.Split('_')[0]), double.Parse(order.Split('_')[1]), maxHash, pool, double.Parse(order.Split('_')[1]),
                        startAmount, refill, isFixedPrice);
                    OrderContainer.Add(orderInstance);
                }

                return;
            }

            maxHash /= numberOrder;
            maxHash = Math.Round(maxHash, 2);

            for (int i = 0; i < numberOrder; i++)
            {

                if (i % 2 == 0)
                {
                    var orderInstance = new OrderInstance(coinLabel, (int)ServerType.EU, price, maxHash, pool, startPrice,
                        startAmount, refill, isFixedPrice);
                    OrderContainer.Add(orderInstance);
                }
                else
                {
                    var orderInstance = new OrderInstance(coinLabel, (int)ServerType.US, price, maxHash, pool, startPrice,
                        startAmount, refill, isFixedPrice);
                    OrderContainer.Add(orderInstance);
                }

            }

            SetColorCoinlabel(coinLabel, Color.GreenYellow);
        }

        private BlockInfo GetLastPOWBlock(IBaseBtcConnector baseBtcConnector)
        {
            var diffPos = 1.0;
            try
            {
                diffPos = float.Parse(ConfigurationManager.AppSettings.Get("DiffPos"));
            }
            catch (Exception ex)
            {
            }

            var crrCount = baseBtcConnector.GetBlockCount();
            var i = 0;
            while (true)
            {
                var hash = baseBtcConnector.GetBlockhash(crrCount - i);
                var block = baseBtcConnector.GetBlock(hash);
                //pow of stack ---> continue
                //if (block.Difficulty < diffPos)
                //if (block.Nonce == "0" && block.Difficulty < diffPos)
                if (String.IsNullOrEmpty(block.Flags) || block.Flags == "proof-of-work")
                {
                    return new BlockInfo()
                    {
                        BlockNumber = crrCount - i,
                        Hash = hash,
                        ServerTimeStamp = block.Time,
                        Diff = block.Difficulty
                    };
                }
                i++;
            }
        }

        private List<BlockInfo> FillList(IBaseBtcConnector baseBtcConnector, int crrCount, int numList, List<BlockInfo> blocks)
        {
            //var diffPos = 1.0;
            //try
            //{
            //    diffPos = float.Parse(ConfigurationManager.AppSettings.Get("DiffPos"));
            //}
            //catch (Exception ex)
            //{
            //}
            var listBlock = new List<BlockInfo>();
            int count = 0;
            int i = 0;
            //for (int i = numList - 1; i >= 0; i--)
            while (count < numList)
            {
                //Get from cache first
                var blockInfo = blocks.SingleOrDefault(b => b.BlockNumber == (crrCount - i));
                if (blockInfo != null)
                {
                    listBlock.Add(blockInfo);
                    i++;
                    count++;
                    continue;
                }
                //If not, then get from rpc
                var hash = baseBtcConnector.GetBlockhash(crrCount - i);
                var block = baseBtcConnector.GetBlock(hash);
                //pow of stack ---> continue
                //if (block.Difficulty < diffPos)
                if (String.IsNullOrEmpty(block.Flags) || block.Flags == "proof-of-work")
                {
                    blockInfo = new BlockInfo()
                    {
                        BlockNumber = crrCount - i,
                        Hash = hash,
                        ServerTimeStamp = block.Time,
                        Diff = block.Difficulty
                    };
                    listBlock.Add(blockInfo);
                    i++;
                    count++;
                    continue;
                }

                i++;
            }
            listBlock.Reverse();
            return listBlock;
        }

        private bool CheckDGWAfterNBlock(List<BlockInfo> blocks, int n, double maxAcceptDiff, int timePerblockSecond)
        {
            var timenow = DateTime.Now;
            var timeStampTimeNowSecond = (int)(timenow.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
            for (int i = 0; i < n; i++)
            {
                timeStampTimeNowSecond += i * 30;
                var nextDiff = DGW(blocks, timePerblockSecond);
                if (nextDiff > maxAcceptDiff)
                {
                    return false;
                }
                blocks = blocks.Skip(1).ToList();
                blocks.Add(new BlockInfo()
                {
                    Diff = nextDiff,
                    ServerTimeStamp = timeStampTimeNowSecond
                });
            }
            return true;
        }

        private Block GetBlock(IBaseBtcConnector baseBtcConnector)
        {
            int i = 0;
            var crrCount = baseBtcConnector.GetBlockCount();
            //for (int i = numList - 1; i >= 0; i--)
            while (true)
            {
                var hash = baseBtcConnector.GetBlockhash(crrCount - i);
                var block = baseBtcConnector.GetBlock(hash);
                //pow of stack ---> continue
                if (block.Difficulty < 1)
                {
                    i++;
                    continue;
                }
                return block;
            }
        }

        private double ForkTarget(List<BlockInfo> blocks, double nTargetTimespan, double nTargetSpacing, bool isNotLimit)
        {
            var nActualSpacing = (double)(blocks[1].ServerTimeStamp - blocks[0].ServerTimeStamp) / 60;
            if (nActualSpacing < 1)
                nActualSpacing = 1;
            if (nActualSpacing > 3 * nTargetSpacing && !isNotLimit)
                nActualSpacing = 3 * nTargetSpacing;
            var nInterval = nTargetTimespan / nTargetSpacing;
            var bnNew = ((nInterval - 1) * nTargetSpacing + nActualSpacing * 2) / ((nInterval + 1) * nTargetSpacing);
            return blocks[1].Diff / bnNew;
        }

        private double EsrcTarget(List<BlockInfo> blocks, double nTargetTimespan, double nTargetSpacing)
        {
            var nInterval = nTargetTimespan / nTargetSpacing;
            if (blocks.Last().BlockNumber == 0)
            {
                blocks.Last().BlockNumber = blocks[blocks.Count - 2].BlockNumber + 1;
            }


            if ((blocks.Last().BlockNumber + 1) % nInterval != 0)
            {
                return blocks.Last().Diff;
            }

            var nActualTimespan = (double)(blocks.Last().ServerTimeStamp - blocks[0].ServerTimeStamp) / 60;

            if (nActualTimespan > 4 * nTargetTimespan)
                nActualTimespan = 4 * nTargetTimespan;
            if (nActualTimespan < nTargetTimespan / 4)
                nActualTimespan = nTargetTimespan / 4;
            return blocks.Last().Diff * nTargetTimespan / nActualTimespan;
        }


        private double DGW(List<BlockInfo> blocks, int timePerBlockSecond)
        {

            double nActualTimespan = 0;
            //nActualTimespan = blocks[numList - 1].ServerTimeStamp - blocks[0].ServerTimeStamp;
            double sumTargets = 0.0;
            for (int i = 0; i < 24; i++)
            {
                if (i > 0)
                {
                    nActualTimespan += blocks[i].ServerTimeStamp - blocks[i - 1].ServerTimeStamp;
                }
                sumTargets += blocks[i].Diff;
                if (i == 23)
                {
                    sumTargets += blocks[i].Diff;
                }
            }
            double nTargetTimespan = 24 * timePerBlockSecond;
            if (nActualTimespan < nTargetTimespan / 3.0)
                nActualTimespan = nTargetTimespan / 3.0;
            if (nActualTimespan > nTargetTimespan * 3.0)
                nActualTimespan = nTargetTimespan * 3.0;

            double compact = (sumTargets / 25) * (nTargetTimespan / nActualTimespan);
            //double compact = (sumTargets / numList) * (nTargetTimespan / nActualTimespan);

            return compact;
        }

        private double _prevBalance;
        private void BalanceRefresh_Tick()
        {
            if (!APIWrapper.ValidAuthorization) return;

            APIBalance Balance = APIWrapper.GetBalance();
            if (Balance == null)
            {
                toolStripLabel2.Text = "";
                _balance = 0.0;
            }
            else
            {
                toolStripLabel2.Text = Balance.Confirmed.ToString("F8") + " BTC";
                _balance = Balance.Confirmed;
            }
            if (_prevBalance != _balance)
            {
                _prevBalance = _balance;
                LoggerInfo.Info($"Balance changed: {_balance}");
            }
        }

        private void TimerRefresh_Tick()
        {
            if (!APIWrapper.ValidAuthorization) return;

            var orders = OrderContainer.GetAll();
            int Selected = -1;

            int listViewCount = 0;

            if (listView1.InvokeRequired)
            {
                listView1.Invoke(new MethodInvoker(delegate { listViewCount = listView1.SelectedIndices.Count; }));
            }

            if (listViewCount > 0)
                listView1.Invoke((Action)delegate
                {
                    Selected = listView1.SelectedIndices[0];
                });

            listView1.Invoke((Action)delegate
            {
                listView1.Items.Clear();
            });
            for (int i = 0; i < orders.Count; i++)
            {
                int Algorithm = orders[i].Algorithm;
                //Location column
                ListViewItem LVI = new ListViewItem(((ServerType)orders[i].ServiceLocation).ToString() + ": " + orders[i].CoinLabel);

                //Algo column
                LVI.SubItems.Add(APIWrapper.ALGORITHM_NAME[Algorithm]);

                if (orders[i].LastOrderStats != null)
                {
                    //OrderId column
                    LVI.SubItems.Add(orders[i].LastOrderStats.ID.ToString());

                    //Price column
                    string PriceText = orders[i].LastOrderStats.Price.ToString("F4") + " (" + orders[i].MaxPrice.ToString("F4") + ")";
                    PriceText += " BTC/" + APIWrapper.SPEED_TEXT[Algorithm] + "/Day";
                    LVI.SubItems.Add(PriceText);

                    //Remaining BTC column
                    LVI.SubItems.Add(orders[i].LastOrderStats.BTCAvailable.ToString("F8"));

                    //Miner column
                    LVI.SubItems.Add(orders[i].LastOrderStats.Workers.ToString());

                    //Speed column
                    string SpeedText = (orders[i].LastOrderStats.Speed * APIWrapper.ALGORITHM_MULTIPLIER[Algorithm]).ToString("F4") + " (" + orders[i].StartLimit.ToString("F2") + ") " + APIWrapper.SPEED_TEXT[Algorithm] + "/s";
                    LVI.SubItems.Add(SpeedText);
                    if (!orders[i].LastOrderStats.Alive)
                        LVI.BackColor = Color.PaleVioletRed;
                    else
                        LVI.BackColor = Color.LightGreen;
                    //Refill column
                    LVI.SubItems.Add(orders[i].IsRefill.ToString());
                    //Fixed column
                    LVI.SubItems.Add(orders[i].IsFixedPrice.ToString());
                    //Dynamic column
                    LVI.SubItems.Add(ListDynamicCoins.Contains(orders[i].CoinLabel).ToString());
                }

                if (Selected >= 0 && Selected == i)
                    LVI.Selected = true;

                listView1.Invoke((Action)delegate
                {
                    listView1.Items.Add(LVI);
                });
            }
        }

        #region Form-Event

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.nicehash.com/index.jsp?p=wallet");
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;

            OrderContainer.Remove(listView1.SelectedIndices[0]);
            TimerRefresh_Tick();
        }
        private void FormMain_Load(object sender, EventArgs e)
        {
            APIWrapper.Initialize(SettingsContainer.Settings.APIID.ToString(), SettingsContainer.Settings.APIKey);
            BalanceRefresh_Tick();
        }

        private void setToUnlimitedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;

            OrderContainer.SetLimit(listView1.SelectedIndices[0], 0);
        }

        private void setToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;

            FormNumberInput FNI = new FormNumberInput("Set new limit", 0, 1000, OrderContainer.GetLimit(listView1.SelectedIndices[0]), 2);
            if (FNI.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                OrderContainer.SetLimit(listView1.SelectedIndices[0], FNI.Value);
            }
        }

        private void setMaximalPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;

            FormNumberInput FNI = new FormNumberInput("Set new price", 0.0001, 100, OrderContainer.GetMaxPrice(listView1.SelectedIndices[0]), 4);
            if (FNI.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                OrderContainer.SetMaxPrice(listView1.SelectedIndices[0], FNI.Value);
            }
        }

        private void setIsRefillToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;
            OrderContainer.SetIsRefill(listView1.SelectedIndices[0]);
        }

        private void setIsFixedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;
            OrderContainer.SetIsFixed(listView1.SelectedIndices[0]);
        }

        private void setCurrentPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 0)
                return;

            FormNumberInput FNI = new FormNumberInput("Set new price", 0.0001, 100, OrderContainer.GetPrice(listView1.SelectedIndices[0]), 4);
            if (FNI.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                OrderContainer.SetPrice(listView1.SelectedIndices[0], FNI.Value);
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }
        #endregion
    }
}
