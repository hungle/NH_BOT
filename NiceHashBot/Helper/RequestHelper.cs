﻿//using CloudFlareUtilities;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace NiceHashBot.Helper
{

    public static class RequestHelper
    {
        public static T RequestApi<T>(string url)
        {
            using (var client = new WebClient())
            {
                try
                {
                    var json = client.DownloadString(url);
                    return JsonConvert.DeserializeObject<T>(json);
                }
                catch (Exception ex)
                {
                    //log error here
                    throw ex;
                    //return default(T);
                }
            }

        }

        //public static async Task<T> RequestApiCloudfare<T>(string url)
        //{
        //    try
        //    {
        //        // Create the clearance handler.
        //        var handler = new ClearanceHandler
        //        {
        //            MaxRetries = 2 // Optionally specify the number of retries, if clearance fails (default is 3).
        //        };

        //        var client = new HttpClient(handler);
        //        // Use the HttpClient as usual. Any JS challenge will be solved automatically for you.
        //        var json = await client.GetStringAsync(url);
        //        return JsonConvert.DeserializeObject<T>(json);
        //    }
        //    catch (AggregateException ex) when (ex.InnerException is CloudFlareClearanceException)
        //    {
        //        throw ex;
        //        // After all retries, clearance still failed.
        //    }
        //    catch (AggregateException ex) when (ex.InnerException is TaskCanceledException)
        //    {
        //        throw ex;
        //        // Looks like we ran into a timeout. Too many clearance attempts?
        //        // Maybe you should increase client.Timeout as each attempt will take about five seconds.
        //    }

        //}
    }
}
