﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceHashBot.Model
{
    public class FtoJson
    {
        public Getpoolstatus getpoolstatus { get; set; }
    }

    public class Getpoolstatus
    {
        public string version { get; set; }
        public float runtime { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public string pool_name { get; set; }
        public float hashrate { get; set; }
        public float efficiency { get; set; }
        public float progress { get; set; }
        public int workers { get; set; }
        public int currentnetworkblock { get; set; }
        public int nextnetworkblock { get; set; }
        public int lastblock { get; set; }
        public float networkdiff { get; set; }
        public float esttime { get; set; }
        public double estshares { get; set; }
        public int timesincelast { get; set; }
        public float nethashrate { get; set; }
    }

}