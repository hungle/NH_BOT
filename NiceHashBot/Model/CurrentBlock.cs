﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceHashBot.Model
{
    public class CurrentBlock
    {
        public string CoinLabel { get; set; }
        public int BlockNumber { get; set; }
    }
}
