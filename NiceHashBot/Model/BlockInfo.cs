﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NiceHashBot.Model
{
    public class BlockInfo
    {
        public int BlockNumber { get; set; }
        public double Diff { get; set; }
        public string Hash { get; set; }
        public long ServerTimeStamp { get; set; }
        public DateTime FoundedServerTime { get; set; }
        public DateTime FoundedLocalTime { get; set; }
    }
}
