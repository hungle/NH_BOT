﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NiceHashBot.Model
{
    public class BlocksCached
    {
        public string CoinLabel { get; set; }
        public List<BlockInfo> ListBlocks { get; set; }
    }
}
