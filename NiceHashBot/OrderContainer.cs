﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using NiceHashBotLib;
using Newtonsoft.Json;
using System.Configuration;
using NLog;

namespace NiceHashBot
{
    public class OrderContainer
    {
        [JsonIgnore]
        private static readonly Logger LoggerInfo = LogManager.GetLogger("info");

        [JsonIgnore]
        public Order OrderStats;

        private static List<OrderInstance> _orderList;

        public OrderContainer()
        {
        }
            
        public static void Initialize()
        {
            _orderList = new List<OrderInstance>();
        }


        public static void Deinitialize()
        {
            foreach (OrderInstance OC in _orderList)
                OC.Stop();
        }


        public static bool IsExist(string coinLabel)
        {
            return _orderList.Any(o => o.CoinLabel.ToLower() == coinLabel.ToLower());
        }

        public static void Add(OrderInstance orderInstance)
        {
            orderInstance.GuidId = new Guid().ToString();
            orderInstance.StartOrder();
            _orderList.Add(orderInstance);
        }


        public static void Remove(int index)
        {
            if (_orderList[index].Stop())
            {
                _orderList.RemoveAt(index);
            }
            //Commit();
        }

        public static void RemoveAll(string coinLabel)
        {
            var orderHere = _orderList.Where(o => o.CoinLabel.ToLower() == coinLabel.ToLower()).ToList();
            for (int i = orderHere.Count - 1; i >= 0; i--)
            {
                if (orderHere[i].Stop())
                {
                    LoggerInfo.Info($"--- Remove order {coinLabel} : #{orderHere[i].OrderId}");
                    _orderList.Remove(orderHere[i]);
                }
            }
        }
        public static void SetLimit(string coinLabel, double limit)
        {
            var orderHere = _orderList.Where(o => o.CoinLabel.ToLower() == coinLabel.ToLower()).ToList();
            foreach (var order in orderHere)
            {
                order.SetLimit(limit);
            }

        }

        public static void SetLimit(int index, double newLimit)
        {
            _orderList[index].SetLimit(newLimit);
            //Commit();
        }

        public static double GetLimit(int index)
        {
            return _orderList[index].StartLimit;
        }
        
        public static void SetMaxPrice(int index, double newMaxPrice)
        {
            _orderList[index].SetMaximalPrice(newMaxPrice);
        }

        public static void SetIsRefill(int index)
        {
            _orderList[index].IsRefill = !_orderList[index].IsRefill;
        }
        public static void SetIsFixed(int index)
        {
            _orderList[index].IsFixedPrice = !_orderList[index].IsFixedPrice;
        }


        public static double GetMaxPrice(int index)
        {
            return _orderList[index].MaxPrice;
        }
        public static double GetPrice(int index)
        {
            return _orderList[index].LastOrderStats.Price;
        }
        public static void SetPrice(int index, double newPrice)
        {
            _orderList[index].SetCurrentPrice(newPrice);
        }

        public static List<OrderInstance> GetAll(string coinLabel = "")
        {
            if (!string.IsNullOrEmpty(coinLabel))
            {
                return _orderList.Where(o => o.CoinLabel.ToLower() == coinLabel.ToLower()).ToList();
            }
            return _orderList;
        }
    }
}
